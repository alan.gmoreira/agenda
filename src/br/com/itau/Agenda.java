package br.com.itau;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Agenda {
    private List<Pessoa> contatos;
    private Pessoa donoAgenda;

    public Agenda(List<Pessoa> contatos, Pessoa donoAgenda) {
        this.contatos = contatos;
        this.donoAgenda = donoAgenda;
    }

    public List<Pessoa> getContatos() {
        return contatos;
    }

    public void setContatos(List<Pessoa> contatos) {
        this.contatos = contatos;
    }

    public Pessoa getDonoAgenda() {
        return donoAgenda;
    }

    public void setDonoAgenda(Pessoa donoAgenda) {
        this.donoAgenda = donoAgenda;
    }

    public boolean addContato(Pessoa pessoa)
    {
        if(this.contatos == null)
            this.contatos = new ArrayList<Pessoa>();

        this.contatos.add(pessoa);

        return true;
    }

    public boolean removerContato(String email)
    {
        contatos.removeIf(pessoa -> pessoa.getEmail().equalsIgnoreCase(email));

        return true;
    }

    public boolean removerContato(int telefone)
    {
        contatos.removeIf(pessoa -> pessoa.getTelefone() == telefone);

        return true;
    }

    public Optional<Pessoa> exibeContato(String email)
    {
        return contatos.stream().filter(pessoa -> pessoa.getEmail() == email).findAny();
    }

    public Optional<Pessoa> exibeContato(int telefone)
    {
        return contatos.stream().filter(pessoa -> pessoa.getTelefone() == telefone).findAny();
    }
}
