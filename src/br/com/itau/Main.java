package br.com.itau;

import jdk.nashorn.internal.ir.WhileNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Scanner scan = new Scanner(System.in);
        List<Pessoa> pessoas = new ArrayList<Pessoa>();

        String saidaContatos = "1";
        String saidaMenu = "1";

        String nome;
        String email;
        int telefone;

        Agenda agenda = null;

        System.out.println("Sistema iniciado.");
        System.out.println("Inclua pessoas na lista");


        while (!saidaContatos.equalsIgnoreCase("q")) {
            System.out.println("Nome");
            nome = scan.next();
            System.out.println("email");
            email = scan.next();
            System.out.println("telefone");
            telefone = scan.nextInt();

            pessoas.add(new Pessoa(nome,email, telefone));

            System.out.println("Digite 'q' para sair ou qualquer outra coisa para adicionar mais pessoas.");
            saidaContatos = scan.next();
        }

        agenda = new Agenda(pessoas, new Pessoa("Alan", "alan.gmoreira@hotmail.com", 231321));

        while (!saidaMenu.equalsIgnoreCase("q")) {
            System.out.println("*****MENU*****");
            System.out.println("1 - Remover por email");
            System.out.println("2 - Remover por telefone");
            System.out.println("3 - Consultar por email");
            System.out.println("4 - Consultar por telefone");

            saidaMenu = scan.next();

            if (saidaMenu.equalsIgnoreCase("1"))
            {
                agenda.removerContato(scan.next());
            }
            else if (saidaMenu.equalsIgnoreCase("2"))
            {
                agenda.removerContato(scan.nextInt());
            }
            else if (saidaMenu.equalsIgnoreCase("3"))
            {
                System.out.println(agenda.exibeContato(scan.next()).toString());
            }
            else if (saidaMenu.equalsIgnoreCase("4"))
            {
                System.out.println(agenda.exibeContato(scan.nextInt()).toString());
            }
        }
    }
}
